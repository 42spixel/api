FROM node:stretch

# Metadata
LABEL mantainer="Gius. Camerlingo <gcamerli@gmail.com>"
LABEL version="1.0"
LABEL description="Nodejs api for Docker."

# Docker image name
ENV NAME=api

# Timezone
ENV TZ="Europe/Paris"

# Update system and install packages
RUN apt-get update
RUN apt-get install -y --no-install-recommends \
  jpegoptim \
  graphicsmagick \
  node-gyp \
  libcups2-dev \
  build-essential \
  libcairo2-dev \
  libpango1.0-dev \
  libjpeg-dev \
  libgif-dev \
  librsvg2-dev

# Clean system
RUN apt-get clean
RUN rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# User
RUN useradd -ms /bin/bash docker

# Home
ENV HOME=/home/docker

# Permissions
RUN chown -R docker:docker $HOME

# Healthcheck
COPY healthcheck /usr/local/bin/
RUN chmod 744 /usr/local/bin/healthcheck

# Change user
USER docker

WORKDIR /web
