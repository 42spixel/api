# **Api**

Node.js api for spixel.

### **MIT License**

This work is licensed under the terms of **[MIT License](https://bitbucket.org/42spixel/api/src/master/LICENSE.md)**.
